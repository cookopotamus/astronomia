/*
 * Copyright (c) 2019 André Schweiger
 * Copyright (c) 2020 Alex Lende
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ajlende.astronomia.accessor;

import java.util.Arrays;
import java.util.UUID;

import com.ajlende.astronomia.AstronomiaMod;

import net.minecraft.network.packet.s2c.play.PlaySoundFromEntityS2CPacket;
import net.minecraft.network.packet.s2c.play.StopSoundS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.registry.Registry;

public class AccessorFabricServer extends AccessorFabric {
	public AccessorFabricServer(UUID playerUUID) {
		super(playerUUID);
	}

	// Player related methods
	@Override
	public boolean updatePlayerInstance() {
		if (AstronomiaMod.server != null)
		player = AstronomiaMod.server.getPlayerManager().getPlayer(playerUUID);
		return player != null;
	}
	
	@Override
	public void playSound(SoundEvent sound, SoundCategory category, float volume, float pitch) {
		//SoundEvent event = new SoundEvent(new Identifier(sound));
		//player.playSound(event, SoundCategory.AMBIENT, volume, pitch); // only works for registered sound events -> only when client has mod installed
		// also only works for registered sound events -> only when client has mod installed, but will bind sounds to player (no longer positional)
		PlaySoundFromEntityS2CPacket packet = new PlaySoundFromEntityS2CPacket(sound, category, player, volume, pitch);
		// PlaySoundIdS2CPacket packet = new PlaySoundIdS2CPacket(Registry.SOUND_EVENT.getId(sound), category, player.getPosVector(), volume, pitch);
		((ServerPlayerEntity)player).networkHandler.sendPacket(packet);
	}

	@Override
	public void stopSound(SoundEvent sound, SoundCategory category) {
		StopSoundS2CPacket packet = new StopSoundS2CPacket(Registry.SOUND_EVENT.getId(sound), category);
		((ServerPlayerEntity)player).networkHandler.sendPacket(packet);
	}
}