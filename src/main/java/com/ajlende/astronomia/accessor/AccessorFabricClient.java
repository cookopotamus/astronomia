/*
 * Copyright (c) 2019 André Schweiger
 * Copyright (c) 2020 Alex Lende
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ajlende.astronomia.accessor;

import java.util.UUID;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.sound.PositionedSoundInstance;
import net.minecraft.client.sound.SoundInstance;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;

public class AccessorFabricClient extends AccessorFabric {
	public AccessorFabricClient(UUID playerUUID) {
		super(playerUUID);
	}

	// Player related methods
	@Override
	public boolean updatePlayerInstance() {
		player = MinecraftClient.getInstance().player;
		return player != null;
	}

	@Override
	public void playSound(SoundEvent sound, SoundCategory category, float volume, float pitch) {
		MinecraftClient.getInstance().getSoundManager().play(new PositionedSoundInstance(sound.getId(), category, volume, pitch, false, 0, SoundInstance.AttenuationType.LINEAR, (float)player.getPosVector().x, (float)player.getPosVector().y, (float)player.getPosVector().z, false));
	}

	@Override
	public void stopSound(SoundEvent sound, SoundCategory category) {
		MinecraftClient.getInstance().getSoundManager().stopSounds(sound.getId(), category);
	}
}
