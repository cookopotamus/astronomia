/*
 * Copyright (c) 2020 Alex Lende
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ajlende.astronomia;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.ajlende.astronomia.accessor.Accessor;
import com.ajlende.astronomia.accessor.AccessorFabricClient;
import com.ajlende.astronomia.accessor.AccessorFabricServer;
import com.ajlende.astronomia.event.PlayerJoinCallback;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.client.ClientTickCallback;
import net.fabricmc.fabric.api.event.server.ServerTickCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;

public class AstronomiaMod implements ModInitializer, ClientModInitializer {
	public static AstronomiaMod instance;

	public static MinecraftServer server;
	public static Set<Accessor> players = new HashSet<Accessor>();

	private boolean runClientSide;

	@Override
	public void onInitialize() {
		System.out.println("init");
		instance = this;

		ServerTickCallback.EVENT.register(server -> {
			AstronomiaMod.server = server;
		});

		PlayerJoinCallback.EVENT.register((connection, player) -> {
			PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
			buf.writeBytes("enabled".getBytes());
			player.networkHandler.sendPacket(new CustomPayloadS2CPacket(new Identifier("astronomia", "server"), buf));

			System.out.println("playerJoin");
			if (player != null) {
				System.out.println(player.getUuidAsString());
				AstronomiaMod.players.add(new AccessorFabricServer(player.getUuid()));
			}
		});
	}

	@Override
	public void onInitializeClient() {
		System.out.println("init client");
		instance = this;

		ClientTickCallback.EVENT.register(client -> {
			if ((client.isIntegratedServerRunning() || client.world == null || client.player == null)
					&& runClientSide) {
				runClientSide = false;
			}
		});
	}

	public void onStartGameSession(MinecraftClient client) {
		if (!runClientSide && !client.isIntegratedServerRunning()) {
			AstronomiaMod.players.add(new AccessorFabricClient(client.player.getUuid()));
			runClientSide = true;

			PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
			buf.writeBytes("astronomia:server".getBytes());
			buf.writeByte(0);
			client.player.networkHandler
					.sendPacket(new CustomPayloadC2SPacket(new Identifier("minecraft", "register"), buf));
		}
	}

	public void onServerModEnabled() {
		if (runClientSide) {
			players.clear();
			runClientSide = false;
		}
	}

	public void broadcastSound(SoundEvent sound, SoundCategory category, float volume, float pitch) {
		players.forEach(player -> {
			System.out.println("broadcast to " + player.playerUUID);
			player.playSound(sound, category, volume, pitch);
		});
	}
}