package com.ajlende.astronomia.mixin;

import com.ajlende.astronomia.AstronomiaMod;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

@Mixin(ServerPlayerEntity.class)
public class MixinServerPlayerEntity {

	@Inject(at = @At("TAIL"), method = "onDeath")
	private void onDeath(DamageSource source, CallbackInfo info) {
        System.out.println("Player Died");
        AstronomiaMod.instance.broadcastSound(SoundEvents.MUSIC_DISC_MELLOHI, SoundCategory.MASTER, 1, 1);
    }
}